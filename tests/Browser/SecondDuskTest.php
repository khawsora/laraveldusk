<?php

namespace Tests\Browser;

use App\Models\Item;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class MySecondDuskTest extends DuskTestCase
{
    use DatabaseMigrations;
    /** @test */
    public function can_view_the_todolist()
    {
        $item = Item::factory()->create();

        $this->browse(function (Browser $browser) use ($item) {
            $browser->visit('/')
                ->pause(1000)
                ->assertSee($item->name);
        });
    }

    /** @test */
    public function can_delete_item_into_the_todolist()
    {
        $item = Item::factory()->create();

        $this->browse(function (Browser $browser) use ($item) {
            $browser->visit('/')
                ->pause(1000)
                ->assertSee($item->name)
                ->click("div > div[class='item item'] > button[class='trashcan']")
                ->pause(1000)
                ->assertDontSee($item->name);
        });
    }
}
