<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class FirstDuskTest extends DuskTestCase
{
    /** @test */
    public function user_can_add_item_into_todolist()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                ->type("name", 'test' )
                ->click("svg[class='svg-inline--fa fa-plus-square fa-w-14 active plus']")
                ->pause(1000)
                ->assertSee('Added');
        });
    }
}
